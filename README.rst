KIOT
=================================

|Join the chat at https://gitter.im/kiot-python/Lobby| |Build Status| |PyPI| |PyPI version|

KIOT is a ..., and was inspired by `this paper <https://>`_.

describe the main idea here

KIOT can be found `on GitHub <https://github.com/>`_.


Benchmarks
----------

All tests were run using ....

+-----------+-----------------------+----------------+---------------+
| Server    | Implementation        | Requests/sec   | Avg Latency   |
+===========+=======================+================+===============+
| Serv 1    | Python 3.5 + RPis     | 33,342         | 2.96ms        |
+-----------+-----------------------+----------------+---------------+


Hello World Example
-------------------

.. code:: python

    from kiot import Kiot

    app = Kiot()


Installation
------------

-  ``python -m pip install kiot``

To install kiot from source ...

- ``git clone https://github.com/``
- ``cd KIOT``
- ``pip install -r requirements.txt``
- ``python setup install``


Installation
------------

Unit tests can be run by using the following command

- ``python -m unittest discover -s <directory> -p '*_test.py'``


Running
-------

You can run KIOT using

- ``python kiot root -c Config/config_root_1.yaml -vvv``
- ``python kiot broker -c Config/config_broker_1.yaml -vvv``
- ``python kiot leaf -c Config/config_leaf_1.yaml -vvv``
- ``python kiot client -c Config/config_client_1.yaml -vvv``


Documentation
-------------

`Documentation on Readthedocs <http://>`_.

.. |Join the chat at https://gitter.im/kiot-python/Lobby| image:: https://badges.gitter.im/kiot-python/Lobby.svg
   :target: https://gitter.im/kiot-python/Lobby?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge
.. |Build Status| image:: https://travis-ci.org/channelcat/kiot.svg?branch=master
   :target: https://travis-ci.org/channelcat/kiot
.. |Documentation| image:: https://readthedocs.org/projects/kiot/badge/?version=latest
   :target: http://sanic.readthedocs.io/en/latest/?badge=latest
.. |PyPI| image:: https://img.shields.io/pypi/v/kiot.svg
   :target: https://pypi.python.org/pypi/kiot/
.. |PyPI version| image:: https://img.shields.io/pypi/pyversions/kiot.svg
   :target: https://pypi.python.org/pypi/kiot/


TODO
----
* something yet to be decided


Limitations
-----------
* probably a lot


THIS README IS SIMPLY A PLACEHOLDER
