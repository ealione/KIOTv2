# -*- coding: utf-8 -*-
import hashlib
import os
import base64
import struct
import binascii
import yaml
import msgpack
import pickle
from hashlib import sha256
from logging import getLogger
from netifaces import interfaces, ifaddresses, AF_INET

__license__ = """
    This file is part of KIOT.

    KIOT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KIOT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KIOT.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Esmerald Aliaj'
__email__ = 'greenleafone7@gmail.com'

_LOG = getLogger(__name__)


def socketid2hex(sid):
    """Returns printable hex representation of a socket id.
    """
    ret = ''.join("%02X" % ord(c) for c in sid)
    return ret


def split_address(msg):
    """Function to split return Id and message received by ROUTER socket.

    Returns 2-tuple with return Id and remaining message parts.
    Empty frames after the Id are stripped.
    """
    ret_ids = []
    for i, p in enumerate(msg):
        if p:
            ret_ids.append(p)
        else:
            break
    return ret_ids, msg[i + 1:]


def hash_b64(s):
    """
    Returns base 64 hash of s
    """
    hasher = sha256(s)
    result = base64.b64encode(hasher.digest())[:-1]
    return result


# TODO: what happens if I give a non existent file
def read_config(
        default_path='config.yaml',
        subsection=None,
        env_key='KIOT_NODE_CFG'
):
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    # if the given path is not absolute then we will assume it is relative to this files' directory
    if not os.path.isabs(path):
        path = '{0}/{1}'.format(os.path.dirname(os.path.realpath(__file__)), path)
    if os.path.exists(path):
        try:
            with open(path, 'rt') as f:
                return yaml.safe_load(f.read()) if subsection is None else yaml.safe_load(f.read())[subsection]
        except yaml.YAMLError as exc:
            _LOG.error("error while reading config file: %s" % exc)
        except KeyError as _:
            _LOG.error("requested key [%s] doesn't exist" % subsection)
    return None


def serialize_object(obj, protocol=-1):
    p = pickle.dumps(obj, protocol)
    return msgpack.packb(p)


def deserialize_object(msg):
    p = msgpack.unpackb(msg)
    return pickle.loads(p)


def bytes_to_hexstring(raw_bytes):
    """
    Returns a readable hexadecimal representation of raw_bytes
    """
    hex_data = binascii.hexlify(raw_bytes)
    text_string = hex_data.decode('utf-8')
    return text_string


def hexstring_to_bytes(hexstring):
    """
    Returns the bytes of hexstring
    """
    raw_bytes = binascii.unhexlify(hexstring.encode('utf-8'))
    return raw_bytes


def get_current_ip(nint=None):
    """
    Will return the current ip address for the given interface.

    If no interface is given the wireless interface will be chosen.

    .. note::

           If the wireless interface name does not start with `w` and
           no explicit interface is given this function will return the
           ip of the first available interface.
    """
    try:
        if nint is None:
            nints = interfaces()
            for cur_nint in nints:
                if cur_nint.startswith('w'):
                    nint = cur_nint
                    break
                else:
                    nint = nints[-1]
        inet = ifaddresses(nint)[AF_INET]
        addr = inet[0]['addr']
        return addr
    except KeyError:
        return None


def get_key_by_value(dictionary, value):
    return list(dictionary.keys())[list(dictionary.values()).index(value)]


def fnv64(data):
    hash_ = 0xcbf29ce484222325
    for b in data:
        hash_ *= 0x100000001b3
        hash_ &= 0xffffffffffffffff
        hash_ ^= b
    return hash_


def hash_fnv64(dn, salt):
    data = salt.encode("ascii") + dn.encode("ascii")
    hash_ = fnv64(data)
    bhash = struct.pack("<Q", hash_)
    return base64.urlsafe_b64encode(bhash)[:-1].decode("ascii")


def hasher(w, type):
    if type == 0:  # faster
        return hashlib.md5(w).hexdigest()[:9]
    elif type == 1:  # smaller size
        return hashlib.md5(w).digest().encode('base64')[:6]
    elif type == 2:  # similar to type 1
        return hashlib.sha256(w).digest().encode('base64')[:6]
    elif type == 3:
        return hash_fnv64(w, 'noncryptographichash')


def random_sensor_data(type):
    # TODO: generate better data
    return "**random data for %s**" % type
