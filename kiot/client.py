# -*- coding: utf-8 -*-
import zmq
import msgpack
from zmq.eventloop.zmqstream import ZMQStream
from zmq.eventloop.ioloop import DelayedCallback
from uuid import uuid4
from logging import getLogger

__license__ = """
    This file is part of KIOT.

    KIOT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KIOT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KIOT.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Esmerald Aliaj'
__email__ = 'greenleafone7@gmail.com'

_LOG = getLogger(__name__)
_DLOG = getLogger('kiot_logger')

KIOT_PROTO = b'KIOTN01'  #: KIOT protocol identifier

_INTEREST_MSG = b'\x01'


class InvalidStateError(RuntimeError):
    """Exception raised when the requested action is not available due to socket state.
    """
    pass


class RequestTimeout(UserWarning):
    """Exception raised when the request timed out.
    """
    pass


class Client(object):
    """Class for the MN client side.

    Thin asynchronous encapsulation of a zmq.REQ socket.
    Provides a :func:`request` method with optional timeout.

    :param context:  the ZeroMQ context to create the socket in.
    :type context:   zmq.Context
    :param endpoint: the endpoint to connect to.
    :type endpoint:  str
    """
    def __init__(self, context, endpoint, n_type, path):
        """Init a Client instance.
        """
        self.path = path
        self.n_type = n_type
        _LOG.name = "%s(%s)" % (self.path.upper().decode('utf-8'), self.n_type.decode('utf-8'))
        self.context = context
        self.endpoint = endpoint
        self.protocol_version = KIOT_PROTO
        self.can_send = True
        self._tmo = None
        self.timed_out = False
        self.stream = None
        self._create_stream()
        return

    def _create_stream(self):
        """Helper for opening a stream.
        """
        socket = self.context.socket(zmq.REQ)
        socket.connect(self.endpoint)
        self.stream = ZMQStream(socket)
        self.stream.on_recv(self._on_message)

    def shutdown(self):
        """Method to deactivate the client connection completely.

        Will delete the stream and the underlying socket.

        .. warning:: The instance MUST not be used after :func:`shutdown` has been called.

        :rtype: None
        """
        if not self.stream:
            return
        self.stream.socket.setsockopt(zmq.LINGER, 0)
        self.stream.socket.close()
        self.stream.close()
        self.stream = None
        return

    def request(self, msg, timeout=None):
        """Send the given message.

        :param msg:     message parts to send.
        :type msg:      list of bytes
        :param timeout: time to wait in milliseconds.
        :type timeout:  int

        :rtype None:
        """
        if not self.can_send:
            # raise InvalidStateError()
            return
        if isinstance(msg, bytes):
            msg = [msg]
        # prepare full message
        # TODO: uuid should be created by the node
        to_send = [self.protocol_version, _INTEREST_MSG, uuid4().bytes]
        to_send.extend(msg)
        if self.stream.closed():
            return
        self.stream.send_multipart(to_send)
        self.can_send = False
        if timeout:
            self._start_timeout(timeout)
        return

    def _on_timeout(self):
        """Helper called after timeout.
        """
        self.timed_out = True
        self._tmo = None
        self.on_timeout()
        return

    def _start_timeout(self, timeout):
        """Helper for starting the timeout.

        :param timeout:  the time to wait in milliseconds.
        :type timeout:   int
        """
        self._tmo = DelayedCallback(self._on_timeout, timeout)
        self._tmo.start()
        return

    def _on_message(self, msg):
        """Helper method called on message receive.

        :param msg:   list of message parts.
        :type msg:    list of str
        """
        if self._tmo:
            # disable timout
            self._tmo.stop()
            self._tmo = None
        # setting state before invoking on_message, so we can request from there
        self.can_send = True
        self.on_message(msg)
        return

    def on_message(self, msg):
        """Public method called when a message arrived.

        .. note:: Does nothing. Should be overloaded!
        """
        pass

    def on_timeout(self):
        """Public method called when a timeout occurred.

        .. note:: Does nothing. Should be overloaded!
        """
        pass
