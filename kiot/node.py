# -*- coding: utf-8 -*-
from logging import getLogger
from sys import getsizeof

import msgpack
import zmq
from zmq.eventloop.ioloop import PeriodicCallback
from zmq.eventloop.zmqstream import ZMQStream

from pybloom import BloomFilter
from util import bytes_to_hexstring, serialize_object, deserialize_object, split_address, random_sensor_data

__license__ = """
    This file is part of KIOT.

    KIOT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KIOT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KIOT.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Esmerald Aliaj'
__email__ = 'greenleafone7@gmail.com'

_LOG = getLogger(__name__)
_DLOG = getLogger('kiot_logger')

KIOT_PROTO = b'KIOTN01'  #: KIOT protocol identifier

HB_INTERVAL = 3000  #: Heartbeat interval in milliseconds
HB_RETRIES = 9  #: Heartbeat retries before quiting

_BF_CAPACITY = 10000
_BF_ERROR = 0.001

_INTEREST_MSG = b'\x01'
_ADVERTISE_MSG = b'\x02'
_NEW_IDENTIFIER_MSG = b'\x03'
_READY = b'\x04'
_HEARTBEAT = b'\x05'
_DISCONNECT = b'\x06'
_AWAKE = b'\x07'
_QUERY_RESPONSE_MSG = b'\x08'

_TYPE_ROOT = b'root'
_TYPE_BROKER = b'broker'
_TYPE_LEAF = b'leaf'

_BIND = b'bind'
_CONNECT = b'connect'


class InvalidStateError(RuntimeError):
    """Exception raised when the requested action is not available due to socket state.
    """
    pass


class RequestTimeout(UserWarning):
    """Exception raised when the request timed out.
    """
    pass


class Node(object):
    """The main KIOT node class.

    This class implements all functionality needed to define and operate a generic node in the KIOT system. It will
    connect to a parent node and provide it with advertisement updates and answer interest queries, and will also
    open a secondary update in order to receive message from child nodes.

    This base class defines the overall functionality and the API. Subclasses are
    meant to implement additional features.

    .. note::

      The parent node will *always* be served by the `sender_endpoint`.

      All child nodes have to use the `receiver_endpoint` and are responsible for
      implementing any subsequent message processing functionality as needed.

    :param context:             the context to use for socket creation.
    :type context:              zmq.Context
    :param sender_endpoint:     the endpoint for sending messages to a parent node.
    :type sender_endpoint:      str
    :param receiver_endpoint:   the endpoint for receiving messages from child nodes.
    :type receiver_endpoint:    str
    :param identifiers:         a list with tags to be used for creating the bftag, used only for leaf nodes.
    :type identifiers:          set
    :param path:                the unique identifier for this node representing it in the logical tree path.
    :type path:                 bytearray
    :param n_type:              logical type of this node depending on where it stands in the tree.
    :type n_type:               bytearray
    :param service_q:           the class to be used for the service request-queue.
    :type service_q:            class

     :Example:

     context = zmq.Context()
     node = Node(context, "tcp://127.0.0.1:9090", "tcp://127.0.0.1:9091", [])
     IOLoop.instance().start()
     node.shutdown()!

     .. seealso:: :class:`NodeRunner`
     .. warnings also:: this class is not yet ready and needs optimization and design review
     .. note:: inherit from this class to create different node types
    """

    # TODO: review documentation
    def __init__(self, context, sender_endpoint, receiver_endpoint, identifiers, path, n_type, service_q=None):
        """Init a Node instance.
        """
        if service_q is None:
            self.service_q = ServiceQueue
        else:
            self.service_q = service_q
        self._child_nodes = {}
        self.context = context
        self.sender_endpoint = sender_endpoint
        self.receiver_endpoint = receiver_endpoint
        self.identifiers = identifiers
        # TODO: How does path fit into all this, also where are we going as a species in general?
        self.path_name = path
        self.ticker = None
        self.curr_retries = HB_RETRIES
        self.type = n_type
        self.data_catalog = {}  # TODO: add statistics, like times information requested
        self._next_hop = {}  # TODO: add a table with the hash of a query as key and the response and timestamp as values for caching
        self._cash_catalog = {}  # query hash, query data
        self._split_parts = {}  # interest query id, split parts number

        _LOG.name = "%s(%s)" % (self.path_name.upper().decode('utf-8'), self.type.decode('utf-8'))

        self.bf_catalog = BloomFilter(capacity=_BF_CAPACITY, error_rate=_BF_ERROR)

        self._msg_types = {_INTEREST_MSG: self.on_interest,
                           _ADVERTISE_MSG: self.on_advertise,
                           _NEW_IDENTIFIER_MSG: self.on_update_identifiers,
                           _READY: self.on_ready,
                           _HEARTBEAT: self.on_heartbeat,
                           _DISCONNECT: self.on_disconnect,
                           _AWAKE: self.on_awake,
                           _QUERY_RESPONSE_MSG: self.on_interest_response,
                           }

        self._parent_msg_types = {_AWAKE: self.on_parent_awake,
                                  _INTEREST_MSG: self.on_interest,
                                  }

        if self.type == _TYPE_ROOT:
            self.parent_active = False
        else:
            self.parent_active = True

        self.sender_stream = None
        if self.type != _TYPE_ROOT:
            self.sender_stream = self.open_stream(self.sender_endpoint, self.on_parent_message, self.path_name,
                                                  _CONNECT)

        # TODO: we should not allow a circular connection
        self.receiver_stream = None
        if self.type != _TYPE_LEAF:
            self.receiver_stream = self.open_stream(self.receiver_endpoint, self.on_message, self.path_name, _BIND)

        # Check child nodes for activity
        self.hb_check_timer = PeriodicCallback(self.on_timer, HB_INTERVAL)
        if self.type != _TYPE_LEAF:
            self.hb_check_timer.start()

        # Send heartbeat to parent node and initiate advertisement
        self.ticker = PeriodicCallback(self._tick, HB_INTERVAL)
        if self.type != _TYPE_ROOT:
            self._send_ready()
            self.ticker.start()

            # Initial node actions
            self.update_bftag(self.identifiers)

    def on_interest(self, rp, msg):
        # TODO: see if it is us who must apply the function
        # TODO: check input for correctness
        uuid = msg.pop(0)
        path = msgpack.unpackb(msg.pop(0))
        func = msgpack.unpackb(msg.pop(0))
        tags = msgpack.unpackb(msg.pop(0))
        if rp:
            # the first ever request will have the id of the client
            client_id = rp[0]
            sender = bytes_to_hexstring(rp[0])
        else:
            client_id = msg.pop(0)
            sender = b'parent'
        _LOG.info('interest message [%s] from [%s] with path %s wanting to apply function(s) %s on %s data' % (
            bytes_to_hexstring(uuid), sender, path, func, tags))
        if self.type == _TYPE_LEAF:
            # return back the requested data
            self.interest_response(uuid, tags, client_id)
            pass  # TODO: implement this
        # as it stands, if the path is wrong then nothing will be returned
        if path is not None:
            if self.path_name in path:
                path.remove(self.path_name)
            # request_hash = hasher(str(path + func + tags), 3)  # TODO: add a hash of the query so we know if we get the same query twice
            # TODO: what if one of those is None
            if len(path):  # TODO: if we split a request wait for getting everything back before sending it
                for p in path:
                    # check if we can use the given path to forward request to the next node
                    if p in self._next_hop:
                        self.interest(self._next_hop[p].id, uuid, path, func, tags, client_id)
        elif len(tags):
            # check if all given tags are in one of the known bf_catalogs
            for bf_catalog in self.data_catalog:
                self.check_bftag(bf_catalog, tags)
                child_trackers = self.data_catalog[bf_catalog]
                for child_tracker in child_trackers:
                    self.interest(child_tracker.id, uuid, path, func, tags, client_id)
        else:
            pass  # information does not exist anymore or is lost
        return

    def on_interest_response(self, rp, msg):
        _LOG.info('new query response from child node [%s]' % bytes_to_hexstring(rp[0]))
        # if root then reply to the client
        if self.type == _TYPE_ROOT:
            uuid = msg.pop(0)
            data = msg.pop(0)
            client_id = msg.pop(0)
            msg = [client_id, b'', KIOT_PROTO, uuid, data]
            if self.receiver_stream.closed():
                return
            self.receiver_stream.send_multipart(msg)
            _LOG.info(
                'responding to client [%s] about [%s]' % (bytes_to_hexstring(client_id), bytes_to_hexstring(uuid)))
            return
        # else propagate response to parent node
        to_send = [b'', KIOT_PROTO, _QUERY_RESPONSE_MSG]
        to_send.extend(msg)
        if self.sender_stream.closed():
            return
        self.sender_stream.send_multipart(to_send)
        pass

    def on_advertise(self, rp, msg):
        """Process an advertisement message.
        """
        _LOG.info('advertise message of %s bytes received from [%s]' % (getsizeof(msg), bytes_to_hexstring(rp[0])))
        if rp[0] not in self._child_nodes:
            # ignore message from unknown child node
            return
        if self._child_nodes[rp[0]] in self.data_catalog.values():
            # remove old record
            self.remove_from_datamap(self._child_nodes[rp[0]].id)
        advertised_bf_catalog = deserialize_object(msg.pop(0))
        if advertised_bf_catalog not in self.data_catalog:
            self.data_catalog[advertised_bf_catalog] = {self._child_nodes[rp[0]]}
        else:
            self.data_catalog[advertised_bf_catalog].add(self._child_nodes[rp[0]])
        self.bf_catalog = self.bf_catalog.union(advertised_bf_catalog)
        self.advertise()
        return

    def on_update_identifiers(self, rp, msg):
        """Process identifiers update message.
        """
        if self.type == _TYPE_LEAF:
            new_identifiers = msgpack.unpackb(msg.pop(0))
            if not isinstance(new_identifiers, list):
                _LOG.debug('identifiers not passed as a list')
                return
            _LOG.debug('adding new identifiers %s' % new_identifiers)
            self.identifiers = self.identifiers.union(set(new_identifiers))
            self.update_bftag(new_identifiers)
        return

    def on_timer(self):
        """Method called on timer expiry.

        Checks which child nodes are dead and unregisters them.

        :rtype: None
        """
        for ntracker in list(self._child_nodes.values()):
            if not ntracker.is_alive():
                _LOG.warning("child node [%s] timed out." % bytes_to_hexstring(ntracker.id))
                self.unregister_node(ntracker.id)
                self.check_node_type()
        return

    def on_ready(self, rp, msg):
        """Process child READY command.

        Registers the child for a service. If this is a leaf node
        then it will be turned into a broker node.

        :param rp:  return address stack
        :type rp:   list of str
        :param msg: message parts
        :type msg:  list of str

        :rtype: None
        """
        self.register_node(rp[0], msg[0], msg[1])
        self.check_node_type()
        return

    def on_heartbeat(self, rp, msg):
        """Process node HEARTBEAT command.

        :param rp:  return address stack
        :type rp:   list of str
        :param msg: message parts
        :type msg:  list of str

        :rtype: None
        """
        ret_id = rp[0]
        try:
            node = self._child_nodes[ret_id]
            if node.is_alive():
                node.on_heartbeat()
        except KeyError:
            pass
        return

    def on_disconnect(self, rp, msg):
        """Process node DISCONNECT command.

        Unregisters the node who sent this message.

        :param rp:  return address stack
        :type rp:   list of str
        :param msg: message parts
        :type msg:  list of str

        :rtype: None
        """
        _LOG.debug("node [%s] wants to disconnect." % bytes_to_hexstring(rp[0]))
        self.unregister_node(rp[0])
        self.check_node_type()
        return

    def on_parent_message(self, msg):
        """Process messages given by parent.

        Decides what kind of message it is and calls the
        appropriate method. If unknown, the message is
        ignored.

        :param msg: message parts
        :type msg:  list of bytearray

        :rtype: None
        """
        self.check_node_type()
        # any message resets the retries counter
        self.curr_retries = HB_RETRIES
        # 1st part is empty
        msg.pop(0)
        # 2nd part is protocol version
        proto = msg.pop(0)
        if proto != KIOT_PROTO:
            _LOG.debug('protocol [%s] is not supported by this node' % proto)
            pass
        # 3rd part is message type
        msg_type = msg.pop(0)
        _LOG.debug('new message from parent with code [%s]' % bytes_to_hexstring(msg_type))
        if msg_type in self._parent_msg_types:
            func = self._parent_msg_types[msg_type]
            func(None, msg)
        else:
            _LOG.debug('message type is not recognized')
        return

    def on_message(self, msg):
        """Processes given message.

        Decides what kind of message it is and calls the
        appropriate method. If unknown, the message is
        ignored.

        :param msg: message parts
        :type msg:  list of bytearray

        :rtype: None
        """
        rp, msg = split_address(msg)
        # 1st part is protocol version
        proto = msg.pop(0)
        if proto != KIOT_PROTO:
            _LOG.debug('protocol [%s] is not supported by this node' % proto)
            pass
        # 2nd part is message type
        msg_type = msg.pop(0)
        _LOG.debug('new message from [%s] with code [%s]' % (bytes_to_hexstring(rp[0]), bytes_to_hexstring(msg_type)))
        if msg_type in self._msg_types:
            func = self._msg_types[msg_type]
            func(rp, msg)
        else:
            _LOG.debug('message type is not recognized')
        return

    def on_request(self, msg):
        """Public method called when a request arrived.

        :param msg:    a list w/ the message parts
        :type msg:     a list of byte-strings

        Must be overloaded to provide support for various services!
        """
        # TODO: will this be used?
        pass

    def on_awake(self, rp, msg):
        """Handle an are you awake message.

        If an already registered child node is sending the message then it will be ignored

        :param rp:     a list w/ the sender ids
        :type rp:      a list of byte-strings
        :param msg:    a list w/ the message parts
        :type msg:     a list of byte-strings
        """
        _LOG.debug("informing child node I am awake")
        send_msg = [rp[0], b'', KIOT_PROTO, _AWAKE]
        if self.receiver_stream.closed() or (rp[0] in self._child_nodes):
            pass
        self.receiver_stream.send_multipart(send_msg)
        return

    def on_parent_awake(self, rp, msg):
        """Handle an I am awake message.

        :param rp:      a list w/ the sender id stack
        :type rp:       a list of byte-strings
        :param msg:     a list w/ the message parts
        :type msg:      a list of byte-strings
        """
        if not self.parent_active:
            _LOG.debug("parent is awake")
            self.curr_retries = HB_RETRIES
            self.check_node_type()
            self._send_ready()
            self.advertise()
        return

    def check_awake(self):
        """Send an `AWAKE` message to parent node.
        """
        if self.sender_stream and (not self.parent_active):
            _LOG.debug("checking if parent is awake")
            send_msg = [b'', KIOT_PROTO, _AWAKE]
            self.sender_stream.send_multipart(send_msg)
        return

    def check_node_type(self):
        """Check if a change in node type should occur.

        If this is a leaf node and it receives a `_READY` request it will become a broker.
        If this is a broker or root node and it loses all child node connections it will turn into a leaf node.
        If this is a broker or leaf node and there is no parent above it then it will become a root node.

        We perform an advertisement of our `bf_tag` each time we move down the tree.

        :rtype: None
        """
        # if I open a root node after its children then it will not receive any advertisements
        # until the childrens' timer runs out
        parent_active = all([self.curr_retries > 0, self.sender_stream])
        children_active = all([len(self._child_nodes) > 0 and self.receiver_stream])
        if self.type == _TYPE_LEAF:
            if not parent_active:
                self._make_root()
            elif parent_active and children_active:
                self._make_broker()
            elif (not parent_active) and (not children_active):
                self._make_root()
        if self.type == _TYPE_ROOT:
            if parent_active and children_active:
                self._make_broker()
            if parent_active and (not children_active):
                self._make_leaf()
            self.check_awake()
        if self.type == _TYPE_BROKER:
            if parent_active and (not children_active):
                self._make_leaf()
                self._send_ready()
                self.advertise()
            if (not parent_active) and children_active:
                self._make_root()
            if (not parent_active) and (not children_active):
                self._make_root()
        return

    def send_disconnect(self, nid):
        """Send disconnect command and unregister node.

        If the node id is not registered, nothing happens.

        :param nid:    the worker id.
        :type nid:     str

        :rtype: None
        """
        try:
            ntracker = self._child_nodes[nid]
        except KeyError:
            return
        _LOG.info("requesting from node [%s] to disconnect" % bytes_to_hexstring(nid))
        to_send = [nid, b'', KIOT_PROTO, _DISCONNECT]

        self.receiver_stream.send_multipart(to_send)
        self.unregister_node(nid)
        return

    def _send_ready(self):
        """Helper method to prepare and send the node READY message.
        """
        _LOG.debug("informing parent I am ready")
        ready_msg = [b'', KIOT_PROTO, _READY, self.type, self.path_name]
        if self.sender_stream.closed():  # TODO: if stream is None this will fail
            pass
        self.sender_stream.send_multipart(ready_msg)
        self.curr_retries = HB_RETRIES
        return

    def _tick(self):
        """Method called every HB_INTERVAL milliseconds.
        """
        self.curr_retries -= 1  # TODO: if this is let to run forever at some point we will reach hell itself
        self.send_hb()
        if self.curr_retries >= 0:
            return
        # connection with parent seems to be dead
        # self.shutdown()
        self.check_node_type()
        return

    def _make_root(self):
        _LOG.info("becoming root node because I lost connection to parent")
        self.parent_active = False
        self.type = _TYPE_ROOT
        self._update_logger_name()
        # if self.ticker.is_running():
        #     self.ticker.stop()

    def _make_leaf(self):
        _LOG.info("becoming leaf node because I lost connection to child nodes and I have a parent node")
        self.parent_active = True
        self.type = _TYPE_LEAF
        self._update_logger_name()
        if self.hb_check_timer.is_running():
            self.hb_check_timer.stop()

    def _make_broker(self):
        _LOG.info("becoming broker node because I have an active connection to parent and child nodes")
        self.parent_active = True
        self.type = _TYPE_BROKER
        self._update_logger_name()
        if not self.hb_check_timer.is_running():
            self.hb_check_timer.start()
        if not self.ticker.is_running():
            self.ticker.start()

    def _update_logger_name(self):
        _LOG.name = '%s(%s)' % (self.path_name.upper().decode('utf-8'), self.type.decode('utf-8'))

    def send_hb(self):
        """Construct and send HB message to parent.
        """
        _LOG.debug("sending heartbeat")
        msg = [b'', KIOT_PROTO, _HEARTBEAT]
        if self.sender_stream.closed():
            pass
        self.sender_stream.send_multipart(msg)
        return

    def disconnect(self, nid):
        """Disconnect from parent.
        """
        _LOG.info("parent wants us to disconnect.")
        self.curr_retries = 0  # reconnect will be triggered by hb timer

    def register_node(self, nid, node_type, node_path):
        """Register the node id and add it to the given service.

        Does nothing if worker is already known.

        :param nid:         the worker id.
        :type nid:          str
        :param node_type:   the type of the node.
        :type node_type:    byte
        :param node_path:   the path identifier of the node.
        :type node_path:    byte

        :rtype: None
        """
        if nid in self._child_nodes:
            return
        node_tracker = LeafTracker(KIOT_PROTO, nid, node_type, node_path, self.receiver_stream)
        self._child_nodes[nid] = node_tracker
        if node_path not in self._next_hop.keys():
            self._next_hop[node_path] = node_tracker
        _LOG.info("new '%s' node registered with id [%s]" % (
            node_type.decode("utf-8"), bytes_to_hexstring(nid)))
        return

    def unregister_node(self, nid):
        """Unregister the node with the given id and stop all timers.

        If the node id is not registered, nothing happens.

        :param nid:    the node id.
        :type nid:     str

        :rtype: None
        """
        try:
            ntracker = self._child_nodes[nid]
        except KeyError:
            return
        ntracker.shutdown()
        self.remove_from_datamap(nid)
        self.clean_dead_links()
        del self._child_nodes[nid]
        del self._next_hop[ntracker.path_name]
        _LOG.info("node [%s] was removed from the pool" % bytes_to_hexstring(nid))
        return

    def remove_from_datamap(self, nid):
        for bf_catalog in self.data_catalog:
            if self._child_nodes[nid] in self.data_catalog[bf_catalog]:
                self.data_catalog[bf_catalog].remove(self._child_nodes[nid])
                break
        return

    def clean_dead_links(self):
        self.data_catalog = {k: v for k, v in self.data_catalog.items() if v}
        self.recreate_bfcatalog()
        return

    def recreate_bfcatalog(self):
        _LOG.debug("recreating bf_catalog")
        self.bf_catalog = BloomFilter(capacity=_BF_CAPACITY, error_rate=_BF_ERROR)
        for bf_catalog in self.data_catalog:
            self.bf_catalog = self.bf_catalog.union(bf_catalog)
        self.advertise()
        pass

    def advertise(self):
        """Send an advertisement to this nodes' parent.

        Nothing will happen if this is a root node
        """
        if self.type != _TYPE_ROOT:
            _LOG.info('advertising `bf tag` to parent node')
            msg = [b'', KIOT_PROTO, _ADVERTISE_MSG, serialize_object(self.bf_catalog)]
            if self.sender_stream.closed():
                pass
            self.sender_stream.send_multipart(msg)
        return

    def interest(self, child_id, uuid, path, func, tags, client_id):
        """Propagate an interest request.
        """
        # TODO: add a queue for each query so if it breaks up somewhere in the way it gets merged and functions can be applied
        msg = [child_id, b'', KIOT_PROTO, _INTEREST_MSG, msgpack.packb(uuid), msgpack.packb(path), msgpack.packb(func),
               msgpack.packb(tags), client_id]
        if self.receiver_stream.closed():
            return
        self.receiver_stream.send_multipart(msg)
        return

    def interest_response(self, uuid, tags, client_id):
        """Respond to a query request.
        """
        _LOG.info(
            "responding to query [%s] initiated by [%s]" % (bytes_to_hexstring(uuid), bytes_to_hexstring(client_id)))
        data = random_sensor_data(tags)  # TODO: return something a tad more useful
        msg = [b'', KIOT_PROTO, _QUERY_RESPONSE_MSG, uuid, msgpack.packb(data), client_id]
        if self.sender_stream.closed():
            return
        self.sender_stream.send_multipart(msg)
        return

    def update_bftag(self, identifiers=None):
        """Insert new identifiers directly to the bgcatalog.
        """
        if identifiers is not None and len(identifiers):
            _LOG.debug('updating `bf tag` with new identifiers %s' % identifiers)
            for identifier in identifiers:
                self.bf_catalog.add(identifier)
            self.advertise()
        return

    def update_path_name(self, new_path_name):
        """Change the path identifier for the current node.
        """
        # TODO: this should also change the socket identifier, if it worked in the first place
        self.path_name = new_path_name

    @staticmethod
    def check_bftag(bf, tags):
        for tag in tags:
            if tag not in bf:
                return False
        return True

    @staticmethod
    def close_stream(stream):
        """Close the given stream.
        """
        if stream and isinstance(stream, ZMQStream):
            stream.socket.close()
            stream.close()
            stream = None
        return

    def open_stream(self, endpoint, callback, identity, connection_type):
        """Open a stream to the given endpoint.

        :param endpoint:        the endpoint to connect to.
        :type endpoint:         str
        :param callback:        the name of the function to be called on a message receive.
        :type callback:         (msg: List[bytearray]) -> None
        :param identity:        the stream identity.
        :type identity:         bytearray
        :param connection_type: connection type can be either `bind` or `connect`.
        :type connection_type:  bytes

        :rtype: ZMQStream
        """
        if connection_type == _CONNECT:
            socket = self.context.socket(zmq.DEALER)
            socket.connect(endpoint)
        elif connection_type == _BIND:
            socket = self.context.socket(zmq.ROUTER)
            socket.bind(endpoint)
        if identity:
            socket.setsockopt(zmq.IDENTITY, identity)
        stream = ZMQStream(socket)
        stream.on_recv(callback)
        return stream

    def shutdown(self):
        """Method to deactivate the node connection completely.

        Will delete the stream and the underlying socket.
        """
        _LOG.debug('deleting all open streams and closing sockets')
        if self.ticker:
            self.ticker.stop()
            self.ticker = None
        if self.hb_check_timer:
            self.hb_check_timer.stop()
            self.hb_check_timer = None
        self.close_stream(self.receiver_stream)
        self.close_stream(self.sender_stream)
        self._child_nodes = None
        self.data_catalog = None
        return


class LeafTracker(object):
    """Helper class to represent a leaf node.

    Instances of this class are used to track the state of the attached leaf
    node and carry the timers for incoming and outgoing heartbeats.

    :param proto:    the worker protocol id.
    :type proto:     bytes
    :param lid:      the leaf node id.
    :type lid:       str
    :param ntype:     the node type could be root, leaf, broker.
    :type ntype:      str
    :param stream:   the ZMQStream used to send messages
    :type stream:    ZMQStream
    """

    def __init__(self, proto, lid, ntype, npath, stream):
        self.proto = proto
        self.id = lid
        self.type = ntype
        self.path_name = npath
        self.curr_retries = HB_RETRIES
        self.stream = stream
        self.last_hb = 0
        self.hb_out_timer = PeriodicCallback(self.send_hb, HB_INTERVAL)
        self.hb_out_timer.start()
        return

    def send_hb(self):
        """Called on every HB_INTERVAL.

        Decrements the current retries count by one.

        Sends heartbeat to leaf node.
        """
        _LOG.debug("sending heartbeat")
        self.curr_retries -= 1
        msg = [self.id, b'', self.proto, _HEARTBEAT]
        if self.stream.closed():
            pass
            # TODO check this
        self.stream.send_multipart(msg)
        return

    def set_stream(self, stream):
        self.stream = stream

    def on_heartbeat(self):
        """Called when a heartbeat message from the leaf node was received.

        Sets current retries to HB_RETRIES.
        """
        self.curr_retries = HB_RETRIES
        return

    def is_alive(self):
        """Returns True when the leaf node is considered alive.
        """
        return self.curr_retries > 0

    def shutdown(self):
        """Cleanup leaf node data.

        Stops timer.
        """
        self.hb_out_timer.stop()
        self.hb_out_timer = None
        self.stream = None
        return


class ServiceQueue(object):
    """Class defining the Queue interface for requests for a service.

    The methods on this class are the only ones used by the node.
    """

    def __init__(self):
        """Initialize queue instance.
        """
        self.q = []
        return

    def __contains__(self, rid):
        """Check if given request id is already in queue.

        :param rid:    the request id
        :type rid:     str
        :rtype:        bool
        """
        return rid in self.q

    def __len__(self):
        """Return the length of the queue.

        :rtype:        int
        """
        return len(self.q)

    def remove(self, rid):
        """Remove a request from the queue.

        :param rid:    the request id
        :type rid:     str
        """
        try:
            self.q.remove(rid)
        except ValueError:
            pass
        return

    def put(self, rid):
        """Put a request in the queue.

        Nothing will happen if the request is already in queue.

        :param rid:    the request id
        :type rid:     str
        """
        if rid not in self.q:
            self.q.append(rid)
        return

    def get(self):
        """Get the next request from the queue.
        """
        if not self.q:
            return None
        return self.q.pop(0)
