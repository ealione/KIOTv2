# -*- coding: utf-8 -*-
import doctest
import zmq
from zmq.eventloop.ioloop import IOLoop
from node import Node
from logging import getLogger

__license__ = """
    This file is part of KIOT.

    KIOT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KIOT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KIOT.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Esmerald Aliaj'
__email__ = 'greenleafone7@gmail.com'

_LOG = getLogger(__name__)
_DLOG = getLogger('kiot_logger')

SENDER = 'Sender'
RECEIVER = 'Receiver'


class NodeRunner(Node):
    def on_request(self, msg):
        # TODO: use this if needed
        pass


def run(sender_endpoint, receiver_endpoint, identifiers, path_name, n_type):
    _LOG.name = "%s(%s)" % (path_name.upper().decode('utf-8'), n_type.decode('utf-8'))
    context = zmq.Context()
    node = NodeRunner(context, sender_endpoint, receiver_endpoint, identifiers, path_name, n_type)
    try:
        IOLoop.instance().start()
    except KeyboardInterrupt:
        _LOG.info("Interrupt received, stopping!")
    finally:
        # clean up
        node.shutdown()
        context.term()
        IOLoop.instance().stop()


if __name__ == '__main__':
    doctest.testmod()
