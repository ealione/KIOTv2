#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import socket
from argparse import ArgumentParser
from logging import config, basicConfig, captureWarnings, getLogger, CRITICAL, ERROR, WARNING, INFO, DEBUG, \
    StreamHandler
from logging.handlers import TimedRotatingFileHandler, RotatingFileHandler

import yaml
import zmq
from zmq.log.handlers import PUBHandler

from _version import __version__
from client_runner import run as cr
from node_runner import run as nr
from util import read_config, get_current_ip

SENDER = 'Sender'
RECEIVER = 'Receiver'
NODE = 'Node'
QUERY = 'Query'
ADDRESS = 'ip'
PORT = 'port'
TRANSPORT = 'transport'
IDENTIFIERS = 'identifiers'
PATH = 'path'
FUNCTIONS = 'functions'
EXTEND = 'extend'
USER = 'user'


# TODO: Fix name format
def kiot_handler():
    log_path = os.path.dirname(os.path.realpath(__file__))
    log_path = os.path.join(log_path, 'Log/kiot.log')
    return TimedRotatingFileHandler(log_path, when="midnight", interval=1, backupCount=20, encoding="utf8")


# TODO: Fix name format
def error_handler():
    log_path = os.path.dirname(os.path.realpath(__file__))
    log_path = os.path.join(log_path, 'Log/errors.log')
    return RotatingFileHandler(log_path, maxBytes=10485760, backupCount=20, encoding="utf8")


# TODO: review this, does the idea make sense
def network_handler(protocol='tcp', endpoint='*', port='4547'):
    ctx = zmq.Context()
    pub = ctx.socket(zmq.PUB)
    try:
        pub.bind('%s://%s:%s' % (protocol, endpoint, port))
    except zmq.error.ZMQError:
        print("Logger::Network logger endpoint is already in use!")
    handler = PUBHandler(pub)
    handler.root_topic = '%s' % get_current_ip()
    return handler


# TODO: add colors maybe
def setup_logging(
        default_path='logging.yaml',
        default_level='INFO',
        env_key='KIOT_LOG_CFG'
):
    cpath = default_path
    value = os.getenv(env_key, None)
    if value:
        cpath = value
    cpath = '{0}/{1}'.format(os.path.dirname(os.path.realpath(__file__)), cpath)
    if os.path.exists(cpath):
        with open(cpath, 'rt') as f:
            configset = yaml.safe_load(f.read())
        config.dictConfig(configset)
    else:
        basicConfig(format='%(asctime)s: %(name)s: %(levelname)s: %(message)s', filename='Log/default.log',
                    level=default_level)
        captureWarnings(True)


if __name__ == "__main__":
    # TODO: Try various things to break this
    parser = ArgumentParser(description='KIOT v%s' % __version__)
    parser.add_argument("m", help="running mode", type=str, default='root',
                        choices=['root', 'leaf', 'broker', 'client'])
    parser.add_argument("-c", "--config", type=str, default='config.yaml',
                        help="the absolute or relative path to a KIOT configuration file")

    # Connection arguments
    parser.add_argument("-l", "--sendport", type=int, default=4545,
                        help="port address to be used for the outgoing stream with this nodes' parent")
    parser.add_argument("-s", "--sendaddress", type=str, default="127.0.0.1",
                        help="main endpoint for the outgoing stream with this nodes' parent")

    parser.add_argument("-k", "--receiveport", type=int, default=4546,
                        help="port address to be used for the incoming stream with this nodes' children")
    parser.add_argument("-r", "--receiveaddress", type=str, default=get_current_ip(),
                        help="main endpoint for the incoming stream with this nodes' children")

    parser.add_argument("-t", "--transport", help="protocol to be used for connections", type=str, default='tcp',
                        choices=['tcp', 'udp', 'inproc'], required=False)

    # Node arguments
    parser.add_argument("-i", "--identifiers", nargs='*',
                        help="space separated list of tags supported by this node, needed only if this is a leaf node",
                        type=str, default=[], required=False)
    parser.add_argument("-p", "--path", help="a unique identifier representing the logical path of this node", type=str,
                        default='n/a', required=False)

    # Client arguments
    parser.add_argument("-u", "--queryuser", help="if running as a client receive queries directly from the terminal",
                        default=False, action="store_true")
    parser.add_argument("-e", "--queryextend", type=int, default=1000, help="time in milliseconds to remake the query")
    parser.add_argument("-w", "--querypath", nargs='*', type=str, default=None,
                        help="blank space separated path to a node based on logical name mapping")
    parser.add_argument("-f", "--queryfunction", nargs='*', type=str, default=None,
                        help="blank space separated list of functions to be applied at the data")
    parser.add_argument("-g", "--querytags", nargs='*', type=str, default=None,
                        help="blank space separated list of tags for the query")

    # Output arguments
    parser.add_argument("-v", "--verbose", help="increase output verbosity", default=0, action="count")
    parser.add_argument("-q", "--quiet", help="suppress all messages to the console", default=False,
                        action="store_true")

    args = parser.parse_args()

    setup_logging()

    try:
        # set logging level
        raw_log_level = args.verbose
        if raw_log_level <= 0:  # default
            log_level = CRITICAL
        elif raw_log_level == 1:
            log_level = ERROR
        elif raw_log_level == 2:
            log_level = WARNING
        elif raw_log_level == 3:
            log_level = INFO
        else:
            log_level = DEBUG
        getLogger().setLevel(log_level)

        # TODO: optimize like its 1997
        if args.quiet:
            # remove the stream handler
            for h in getLogger().handlers:
                if isinstance(h, StreamHandler):
                    getLogger().removeHandler(h)

        # read the configuration file if any is given
        file_configs = read_config(args.config)

        # TODO: config file arguments take precedence, should it be command line arguments that do though
        if file_configs:
            sendaddress = file_configs.get(SENDER, {}).get(ADDRESS)
            if sendaddress:
                args.sendaddress = sendaddress

            receiveaddress = file_configs.get(RECEIVER, {}).get(ADDRESS)
            if receiveaddress:
                args.receiveaddress = receiveaddress

            sendport = file_configs.get(SENDER, {}).get(PORT)
            if sendport:
                args.sendport = sendport

            receiveport = file_configs.get(RECEIVER, {}).get(PORT)
            if receiveport:
                args.receiveport = receiveport

            transport = file_configs.get(NODE, {}).get(TRANSPORT)
            if transport:
                args.transport = transport

            identifiers = file_configs.get(NODE, {}).get(IDENTIFIERS)
            if identifiers:
                args.identifiers.extend(identifiers)

            path = file_configs.get(NODE, {}).get(PATH)
            if path:
                args.path = path

            if args.m == 'client':
                queryuser = file_configs.get(NODE, {}).get(USER)
                if queryuser:
                    args.queryuser = queryuser

                queryextend = file_configs.get(QUERY, {}).get(EXTEND)
                if queryextend >= 0:
                    args.queryextend = queryextend

                querypath = file_configs.get(QUERY, {}).get(PATH)
                if isinstance(querypath, list):
                    args.querypath = querypath

                queryfunction = file_configs.get(QUERY, {}).get(FUNCTIONS)
                if isinstance(queryfunction, list):
                    args.queryfunction = queryfunction

                querytags = file_configs.get(QUERY, {}).get(IDENTIFIERS)
                if isinstance(querytags, list):
                    args.querytags = querytags
        else:
            print("no configuration file could be parsed, using provided arguments or defaults")

        if (args.sendport < 4000 or args.sendport > 65535) and (args.receiveport < 4000 or args.receiveport > 65535):
            parser.error("--ports should be a valid port number in the range (4000, 65535)")

        # Check if IPs are valid.
        socket.inet_aton(args.sendaddress)
        socket.inet_aton(args.receiveaddress)

        sender_endpoint = '%s://%s:%s' % (args.transport, args.sendaddress, args.sendport)
        receiver_endpoint = '%s://%s:%s' % (args.transport, args.receiveaddress, args.receiveport)

        if args.m == 'root':
            sender_endpoint = None
        elif args.m == 'leaf' or args.m == 'client':
            receiver_endpoint = None

        _LOG = getLogger("%s(%s)" % (args.path.upper(), args.m))
        _DLOG = getLogger('kiot_logger')

        _LOG.info("-KIOT v%s-" % __version__)
        _LOG.info("running as a -%s- node connected at [%s] and listening at [%s]" % (
            args.m, sender_endpoint, receiver_endpoint))

        if args.m != 'client':
            nr(sender_endpoint, receiver_endpoint, set(args.identifiers), bytes(args.path, encoding='utf-8'),
               bytes(args.m, encoding='utf-8'))
        else:
            cr(sender_endpoint, bytes(args.path, encoding='utf-8'), bytes(args.m, encoding='utf-8'), args.queryuser,
               args.queryextend, args.querypath, args.queryfunction, args.querytags)

    except KeyboardInterrupt:
        pass
    except (socket.error, TypeError):
        parser.error("Endpoints should be a valid IPv4 or IPv6 address")
        pass
