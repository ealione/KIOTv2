# -*- coding: utf-8 -*-
from node_runner import run as run_node
from node import Node, InvalidStateError, RequestTimeout
from logging import getLogger

__license__ = """
    This file is part of KIOT.

    KIOT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KIOT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KIOT.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Esmerald Aliaj'
__email__ = 'greenleafone7@gmail.com'

_LOG = getLogger(__name__)
_DLOG = getLogger('kiot_logger')


class Kiot:
    def __init__(self):
        pass
