# -*- coding: utf-8 -*-
import doctest
import msgpack
import re
import zmq
from zmq.eventloop.ioloop import IOLoop
from zmq.eventloop.ioloop import PeriodicCallback
from timeit import default_timer as timer
from client import Client
from logging import getLogger
from util import get_current_ip, bytes_to_hexstring

__license__ = """
    This file is part of KIOT.

    KIOT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KIOT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KIOT.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Esmerald Aliaj'
__email__ = 'greenleafone7@gmail.com'

_LOG = getLogger(__name__)
_DLOG = getLogger('kiot_logger')

TIMEOUT = 2000


class ClientRunner(Client):
    def __init__(self, context, endpoint, n_type, path, interactive, repeat, querypath, queryfunction, querytags):
        Client.__init__(self, context, endpoint, n_type, path)
        self.repeat = repeat
        self.query_timer = PeriodicCallback(self.interest_query, repeat)
        self.querypath = None
        self.queryfunction = None
        self.querytags = None
        self.timer = 0
        if interactive:
            self.user_loop()
        else:
            self.querypath = querypath
            self.queryfunction = queryfunction
            self.querytags = querytags
            if repeat > 1000:
                self.query_timer.start()
            else:
                self.interest_query()

    def on_message(self, msg):
        kiot_proto = msg.pop(0)
        uuid = msg.pop(0)
        data = msgpack.unpackb(msg.pop(0))
        _LOG.info("received [%s] for query [%s] after %.4f seconds" % (data, bytes_to_hexstring(uuid), timer() - self.timer))
        if self.repeat <= 0:
            IOLoop.instance().stop()
        return

    def on_timeout(self):
        _LOG.info('timeout! no response after %s msec' % TIMEOUT)
        if self.query_timer.is_running():
            self.query_timer.stop()
        IOLoop.instance().stop()
        return

    def interest_query(self, path=None, func=None, tags=None):
        if path is None:
            path = self.querypath
        if func is None:
            func = self.queryfunction
        if tags is None:
            tags = self.querytags
        _LOG.info('sending new interest query {path: %s, func: %s, tags: %s}' % (path, func, tags))
        msg = [msgpack.packb(path), msgpack.packb(func), msgpack.packb(tags)]
        self.request(msg, TIMEOUT)
        self.timer = timer()
        return

    def user_loop(self):
        print("\n--Query creation tool (enter each new item separated by space)--")
        while True:
            querypath, queryfunction, querytags = self.build_query()
            if (querytags is None) and (querypath is None):
                print("no data can be returned from this query, try again")
                if self.get_input_bool():
                    continue
                break
            print('new interest query {path: %s, func: %s, tags: %s}' % (querypath, queryfunction, querytags))
            if self.get_input_bool():
                self.interest_query(querypath, queryfunction, querytags)  # TODO: not working
            print("perform a new query?")
            if not self.get_input_bool():
                break
            pass
        self.shutdown()
        self.context.term()
        IOLoop.instance().stop()

    def build_query(self):
        querypath = self.get_input_list('path')
        queryfunction = self.get_input_list('functions')
        querytags = self.get_input_list('tags')
        return querypath, queryfunction, querytags

    @staticmethod
    def get_input_list(qtype):
        retry_count = 3
        while True:
            if retry_count <= 0:
                return None
            user_input = input("enter query %s (leave blank to skip) >> " % qtype)
            if len(user_input) > 200:
                print("input is too big, try again (%s)" % retry_count)
                retry_count = retry_count - 1
                continue
            elif not re.match("^[A-Za-z0-9\s_-]*$", user_input):
                print("only letters, numbers, underscores, and dasher are allowed, try again (%s)" % retry_count)
                retry_count = retry_count - 1
                continue
            else:
                break
        return user_input.split(" ") if user_input != '' else [None]

    @staticmethod
    def get_input_bool():
        retry_count = 3
        while True:
            if retry_count <= 0:
                return False
            user_input = input("proceed [yes/no]? >> ")
            if len(user_input) > 3:
                print("input is too big, try again (%s)" % retry_count)
                retry_count = retry_count - 1
                pass
            if re.match(r'(?i)yes', user_input):
                return True
            elif re.match(r'(?i)no', user_input):
                return False
            else:
                print("input was not understood, try again (%s)" % retry_count)
                retry_count = retry_count - 1
                pass


def run(endpoint, path, n_type, interactive, repeat, querypath, queryfunction, querytags):
    _LOG.name = "%s(%s)" % (path.upper().decode('utf-8'), n_type.decode('utf-8'))
    context = zmq.Context()
    client = ClientRunner(context, endpoint, n_type, path, interactive, repeat, querypath, queryfunction, querytags)
    try:
        IOLoop.instance().start()
    except KeyboardInterrupt:
        _LOG.info("Interrupt received, stopping!")
    finally:
        # clean up
        client.shutdown()
        context.term()
        IOLoop.instance().stop()


if __name__ == "__main__":
    doctest.testmod()
    run("tcp://%s:9091".format(get_current_ip()), "test", b'client', False, 0, ["citynode"], [None], ["temp"])
