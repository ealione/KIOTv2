#!/bin/bash
#===============================================================================
#
#          FILE:  test_run.sh
#
#         USAGE:  ./test_run.sh
#
#   DESCRIPTION:  This script will scan the `config` directory and run as many instances of KIOT as there are
#                 configuration files in that directory. It is assumed that the configuration files are named following
#                 the format <name>_<node_type>_<id>.yaml
#
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  Esmerald Aliaj, greenleafone7@gmail.com
#   AFFILIATION:  AUEB
#       VERSION:  1.0
#       CREATED:  01/01/2018 09:10:01 PM UTF
#      REVISION:  ---
#===============================================================================

program_name=$0

usage() {
    cat <<EOM
    Usage:
    $(basename $0) {start|stop} -o <output_file>

   -o  File to write the log output (does nothing right now)

EOM
    exit 0
}

run_instance() {
    t=`echo $1 | awk '{split($0, arr, "[__]"); print arr[2]}'`
    echo "----Running a $t instance using 'kiot/Config/$1'"
    python3 kiot ${t} -c Config/$1 -vvv &
}

setup_nodes() {
    echo "--$1 nodes"
    for i in kiot/Config/*_$1_*.yaml; do
        [ -f "$i" ] || break
        b=$(basename ${i})
        run_instance ${b}
    done
}

setup() {
    echo "Setting up KIOT instances"
    setup_nodes root
    setup_nodes broker
    setup_nodes leaf
    setup_nodes client
    echo "Done"
}

kill_all() {
    echo "Stopping KIOT instances"
    for i in `pgrep -f "python3 kiot"`; do
        echo "  Killing process with id $i"
        kill -9 ${i}
    done
    echo "Done"
}

case "$1" in
  start)
    setup
    ;;
  stop)
    kill_all
    ;;
  *)
    usage
    exit 1
    ;;
esac

exit 0
